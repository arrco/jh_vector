# jh_vector

## 介绍

jh_vector是一个动态数组。实现的功能有点类似于 C++ STL vector容器。

## 使用说明

只需要包含 jh_vector.h 单个头文件即可使用。



## 使用示例

基础用法：

```c
int main(int argc, char* argv[])
{
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : %d\n", jh_vector_size(array));
    
    //往数组末尾增加数值20
    jh_vector_push_back(array, 20);
    
    //查看当前数组大小
    printf("size : %d\n", jh_vector_size(array));
    
    //删除数组的最后一个数据
    jh_vector_pop_back(array);

    //查看当前数组大小
    printf("size : %d\n", jh_vector_size(array));
    
    return 0;
}
```

运行结果如下：

```shell
0,1,2,3,4,5,6,7,8,9,
size : 10
size : 11
size : 10
```



## 接口使用说明

### 1.定义动态数组

#### 方式一

调用接口`jh_vector_type(type)`定义动态数组。

参数含义：

| 参数 | 含义     |
| ---- | -------- |
| type | 数据类型 |

定义不同数据类型的动态数组示例：

```c
//定义动态数组的数据类型int和变量名array
jh_vector_type(int) array;

//定义动态数组的数据类型char和变量名buffer
jh_vector_type(char) buffer;

//定义动态数组的数据类型struct test和变量名buffer
struct test{
    int key;
    int val;
};
    
jh_vector_type(struct test) buff;
```



#### 方式二

调用接口`jh_vector_global_type(type)`定义全局的动态数组。当要定义全局的动态数组时，需要使用该接口而不是`jh_vector_type(type)`，并且在不使用该动态数组时，调用`jh_vector_destroy`接口进行销毁。

参数含义：

| 参数 | 含义     |
| ---- | -------- |
| type | 数据类型 |

使用示例：

```c
//定义动态数组的数据类型int和变量名array
jh_vector_global_type(int) array;

//初始化动态数组
jh_vector_init(array, 10);

//销毁动态数组
jh_vector_destroy(array);
```



#### 方式三

调用接口`jh_vector_type_name`定义动态数组并初始化，动态数组长度为0。使用该接口来定义动态数组，则后续可以不用调用 `jh_vector_init`初始化。

```c
/*定义动态数组并初始化*/
jh_vector_type_name(type, name);
```

参数含义：

| 参数 | 含义     |
| ---- | -------- |
| type | 数据类型 |
| name | 变量名   |

使用示例：

```c
//定义动态数组的数据类型int和变量名array
jh_vector_type_name(int, array);
```



#### 方式四

调用接口`jh_vector_global_type_name`定义全局的动态数组并初始化，动态数组长度为0。当要定义全局的动态数组时，需要使用该接口而不是`jh_vector_type_name`，并且在不使用该动态数组时，调用`jh_vector_destroy`接口进行销毁。

```c
/*定义全局的动态数组并初始化*/
jh_vector_global_type_name(type, name);
```

参数含义：

| 参数 | 含义     |
| ---- | -------- |
| type | 数据类型 |
| name | 变量名   |

使用示例：

```c
//定义动态数组的数据类型int和变量名array
jh_vector_global_type_name(int, array);

//销毁动态数组
jh_vector_destroy(array);
```



### 2.动态数组初始化

调用接口`jh_vector_init`初始化动态数组。接口`jh_vector_init`可以通过以下三种方式初始化：

```c
/*初始化动态数组，默认长度为0*/
jh_vector_init(vector);
/*初始化动态数组，设置数组长度为n*/
jh_vector_init(vector, n);
/*初始化动态数组，设置数组长度为n，初始化数值为value*/
jh_vector_init(vector, n, value);
```

初始化接口只能调用一次，否则会造成内存泄露。如果要重新初始化，需要调用`jh_vector_reinit`接口来重新初始化。

参数含义：

| 参数   | 含义                        |
| ------ | --------------------------- |
| vector | 动态数组                    |
| n      | 动态数组长度，必须大于等于0 |
| value  | 动态数组初始值              |

使用示例展示3种不同的初始化方式：

```c
//定义动态数组的数据类型int和变量名array
jh_vector_type(int) array;

//初始化动态数组，长度为0
jh_vector_init(array);
//初始化动态数组，长度为10，初始值未知
jh_vector_init(array, 10);
//初始化动态数组，长度为10，初始值为8
jh_vector_init(array, 10, 8);
```



### 3.动态数组读写数据

调用接口`jh_vector_at`读写数据。动态数组读写数据的方式和操作普通数组的方式一致。

```c
/*读写数据*/
jh_vector_at(vector, i);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |
| i      | 索引值   |

使用示例展示读写动态数组数据：

```c
//循环赋值
for(int i = 0; i < 10; i++) {
    jh_vector_at(array, i) = i;
}

//打印出数组中的数值
for(int i = 0; i < 10; i++) {
    printf("%d,\n", jh_vector_at(array, i));
}
```



### 4.查看动态数组的大小

调用接口`jh_vector_size`查看动态数组的大小。

```c
/*查看动态数组的大小*/
jh_vector_size(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义               |
| ------ | ------------------ |
| len    | 返回动态数组的大小 |

使用示例：

```c
//查看动态数组大小
printf("size : %d\n", jh_vector_size(array));
```



### 5.在数组的最后添加一个数据

调用接口`jh_vector_push_back`在数组的最后添加一个数据。这个接口会动态增长数组的大小，假如当前数组大小为10，在数组的最后添加一个数据后，数组的大小会变成11；假如当前数组大小为空，添加一个数据后，数组的大小会变成1，成为数组的首个元素。

```c
/*在数组的最后添加一个数据*/
jh_vector_push_back(vector, value);
```

参数含义：

| 参数   | 含义         |
| ------ | ------------ |
| vector | 动态数组     |
| value  | 要添加的数值 |

使用示例：

```c
//定义动态数组的数据类型int和变量名array
jh_vector_type(int) array;
//初始化动态数组，长度为10
jh_vector_init(array, 10);

//循环赋值
for(int i = 0; i < jh_vector_size(array); i++) {
    jh_vector_at(array, i) = i;
}

//打印出数组中的数值
for(int i = 0; i < jh_vector_size(array); i++) {
    printf("%d ", jh_vector_at(array, i));
}
printf("\n");

//查看当前数组大小
printf("size : %d\n", jh_vector_size(array));

//往数组末尾增加数值20
jh_vector_push_back(array, 20);

//打印出数组中的数组
for(int i = 0; i < jh_vector_size(array); i++) {
    printf("%d ", jh_vector_at(array, i));
}
printf("\n");

//查看当前数组大小
printf("size : %d\n", jh_vector_size(array));
```

运行结果：

```
0 1 2 3 4 5 6 7 8 9
size : 10
0 1 2 3 4 5 6 7 8 9 20
size : 11
```



### 6.删除数组的最后一个数据

调用接口`jh_vector_pop_back`删除数组的最后一个数据。这个接口会动态减少数组的大小，假如当前数组大小为10，删除最后一个数据后，数组的大小会变成9；数组为空时不能删除数据。该接口会返回删除的数组最后一个数据的值。

```c
/*删除数组的最后一个数据*/
jh_vector_pop_back(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义                           |
| ------ | ------------------------------ |
| value  | 返回删除的数组最后一个数据的值 |

基于上一个使用示例，添加如下部分：

```c
//删除数组的最后一个数据
jh_vector_pop_back(array);

//打印出数组中的数组
for(int i = 0; i < jh_vector_size(array); i++) {
    printf("%d ", jh_vector_at(array, i));
}
printf("\n");

//查看当前数组大小
printf("size : %d\n", jh_vector_size(array));
```

运行结果：

```
0 1 2 3 4 5 6 7 8 9
size : 10
```



### 7.获取动态数组数据的地址

#### 方式一

调用接口`jh_vector_data`获取动态数组数据的地址。

```c
/*获取动态数组第1个数据的地址*/
jh_vector_data(vector);
/*获取动态数组第i个数据的地址*/
jh_vector_data(vector, i);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |
| i      | 索引值   |

返回值：

| 返回值 | 含义                   |
| ------ | ---------------------- |
| ptr    | 返回指向数据地址的指针 |

使用示例：

```c
//获取数组数据的地址，使用memset函数将所有内容设置为0
memset(jh_vector_data(array), 0x00, sizeof(int) * jh_vector_size(array));

//获取第5个元素的地址，
int* val = jh_vector_data(array, 5);
```



#### 方式二

使用`&`来获取地址值。

```c
/*获取地址*/
&jh_vector_at(vector, i);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |
| i      | 索引值   |

使用示例：

```c
//获取第6个元素的地址
int* val = &jh_vector_at(array, 6);
```



### 8.获取首个数据的值

调用接口`jh_vector_front`获取首个数据的值。

```c
/*获取首个数据的值*/
jh_vector_front(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义                     |
| ------ | ------------------------ |
| value  | 返回动态数组首个数据的值 |

使用示例：

```c
printf("front %d\n", jh_vector_front(array));
```



### 9.获取最后一个数据的值

调用接口`jh_vector_back`获取最后一个数据的值。

```c
/*获取最后一个数据的值*/
jh_vector_back(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义                         |
| ------ | ---------------------------- |
| value  | 返回动态数组最后一个数据的值 |

使用示例：

```c
printf("back %d\n", jh_vector_back(array));
```



### 10.获取首个数据的索引值

调用接口`jh_vector_begin`获取首个数据的索引值。

```c
/*获取首个数据的值*/
jh_vector_begin(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义                         |
| ------ | ---------------------------- |
| value  | 返回动态数组首个数据的索引值 |

使用示例：

```c
//打印出数组中的数值
for(int i = jh_vector_begin(array); i < jh_vector_end(array); i++) {
    printf("%d,", jh_vector_at(array, i));
}
printf("\n");
```



### 11.获取最后一个数据的索引值

调用接口`jh_vector_end`获取最后一个数据的索引值加1的值。

```c
/*获取最后一个数据的索引值加1的值*/
jh_vector_end(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义                                    |
| ------ | --------------------------------------- |
| value  | 返回动态数组最后一个数据的索引值加1的值 |

使用示例：

```c
//打印出数组中的数值
for(int i = jh_vector_begin(array); i < jh_vector_end(array); i++) {
    printf("%d,", jh_vector_at(array, i));
}
printf("\n");
```



### 12.判断动态数组是否为空

调用接口`jh_vector_empty`判断动态数组是否为空，也就是数组大小为0。

```c
/*判断动态数组是否为空*/
jh_vector_empty(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义                      |
| ------ | ------------------------- |
| true   | 动态数组为空时返回true    |
| false  | 动态数组不为空时返回false |

使用示例：

```c
printf("array %s empty\n", jh_vector_empty(array) ? "is" : "isn't");
```



### 13.调整动态数组的大小

调用接口`jh_vector_resize`调整动态数组的大小。

```c
/*调整动态数组的大小, 大小修改为n*/
jh_vector_resize(vector, n);
/*调整动态数组的大小, 大小修改为n, 增加的部分数值设置为value*/
jh_vector_resize(vector, n, value);
```

参数含义：

| 参数   | 含义                                              |
| ------ | ------------------------------------------------- |
| vector | 动态数组                                          |
| n      | 动态数组长度，必须大于等于0                       |
| value  | 动态数组如果长度增加，则增加的部分数值设置为value |

使用示例：

```c
//调整动态数组的大小变为8
jh_vector_resize(array, 8);

//调整动态数组的大小变为16，增加的部分数值设置为66
jh_vector_resize(array, 16, 66);
```



### 14.清空动态数组

调用接口`jh_vector_resize`清空动态数组，动态数组的大小变为0。

```c
/*清空动态数组*/
jh_vector_clear(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

使用示例：

```c
jh_vector_clear(vector);
```



### 15.修改动态数组的数值

调用接口`jh_vector_modify`修改动态数组的数值。

```c
/*将动态数组索引值为position的数值修改为value*/
jh_vector_modify(vector, position, value);

/*将动态数组索引值范围为[first, last)的数值修改为value*/
jh_vector_modify(vector, first, last, value);
```

参数含义：

| 参数     | 含义                            |
| -------- | ------------------------------- |
| vector   | 动态数组                        |
| position | 索引值                          |
| value    | 要修改的数值                    |
| first    | 起始的索引值，范围[first, last) |
| last     | 结束的索引值，范围[first, last) |

使用示例：

```c
//第5个元素的数值修改为5
jh_vector_modify(array, 5, 55);

//范围为[5, 12)的数值修改为66
jh_vector_modify(array, 5, 12, 66);
```



### 16.在动态数组中插入数据

调用接口`jh_vector_insert`在动态数组中插入数据。动态数组的大小会在插入数据后变大。

```c
/*在动态数组索引值为position的位置上插入数据value*/
jh_vector_insert(vector, position, value);

/*从动态数组索引值为position的位置开始插入n个数据value*/
jh_vector_insert(vector, position, n, value);

/*将动态数组vector2索引值范围为[first, last)的数值，插入到动态数组vector1索引值为position开始的位置，注意vector1和vector2动态数组的数据类型必须相同*/
jh_vector_insert(vector1, position, vector2, first, last);
```

参数含义：

| 参数     | 含义                            |
| -------- | ------------------------------- |
| vector   | 动态数组                        |
| position | 索引值                          |
| value    | 要修改的数值                    |
| n        | 要插入的数据长度，必须大于等于0 |
| first    | 起始的索引值，范围[first, last) |
| last     | 结束的索引值，范围[first, last) |

使用示例：

```c
//在索引值为3的位置上插入数据66，假设插入数组数据为[0,1,2,3,4],插入后变为[0,1,2,66,3,4]
jh_vector_insert(array, 3, 66);

//在索引值为3的位置上插入2个数据77，假设插入数组数据为[0,1,2,3,4],插入后变为[0,1,2,77,77,3,4]
jh_vector_insert(array, 3, 2, 77);

//将动态数组array索引值范围为[1, 5)的数值，插入到动态数组array2索引值为0开始的位置
jh_vector_insert(array2, 0, array, 1, 5);
```



### 17.在动态数组中删除数据

调用接口`jh_vector_erase`在动态数组中删除数据。动态数组的大小会在删除数据后变小。

```c
/*删除动态数组索引值为position的位置上的数据*/
jh_vector_erase(vector, position);

/*删除动态数组索引值范围为[first, last)的数据*/
jh_vector_erase(vector, first, last);
```

参数含义：

| 参数     | 含义                            |
| -------- | ------------------------------- |
| vector   | 动态数组                        |
| position | 索引值                          |
| first    | 起始的索引值，范围[first, last) |
| last     | 结束的索引值，范围[first, last) |

使用示例：

```c
//删除索引值为3的位置上的数据，假设数组数据为[0,1,2,3,4],删除后变为[0,1,2,4]
jh_vector_erase(array, 3);

//删除动态数组索引值范围为[1, 3)的数据，假设数组数据为[0,1,2,3,4],删除后变为[0,3,4]
jh_vector_erase(array, 1, 3);
```



### 18.复制动态数组中的数据

调用接口`jh_vector_copy`复制动态数组中的数据。

```c
/*将动态数组vector2的所有数据复制到动态数组vector1中*/
jh_vector_copy(vector1, vector2);

/*将动态数组vector2的前n个数据复制到动态数组vector1中*/
jh_vector_copy(vector1, vector2, n);

/*将动态数组vector2索引值范围为[first, last)的数据复制到动态数组vector1中*/
jh_vector_copy(vector1, vector2, first, last);

/*将动态数组vector2索引值范围为[first, last)的数据复制到动态数组vector1索引值为position开始的位置*/
jh_vector_copy(vector1, position, vector2, first, last);
```

参数含义：

| 参数     | 含义                            |
| -------- | ------------------------------- |
| vector   | 动态数组                        |
| n        | 要复制的数据长度，必须大于等于0 |
| first    | 起始的索引值，范围[first, last) |
| last     | 结束的索引值，范围[first, last) |
| position | 索引值                          |

使用示例：

```c
/*将动态数组array的所有数据复制到动态数组array2中*/
jh_vector_copy(array2, array);

/*将动态数组array的前5个数据复制到动态数组array3中*/
jh_vector_copy(array3, array, 5);

/*将动态数组array索引值范围为[5, 8)的数据复制到动态数组array4中*/
jh_vector_copy(array4, array, 5, 8);

/*将动态数组array索引值范围为[5, 8)的数据复制到动态数组array5索引值为0开始的位置*/
jh_vector_copy(array5, 0, array, 5, 8);
```



### 19.交换两个动态数组中的数据

调用接口`jh_vector_swap`交换两个动态数组中的数据。

```c
/*交换动态数组vector1和动态数组vector2中的数据*/
jh_vector_swap(vector1, vector2);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

使用示例：

```c
//交换array和array2中的数据
jh_vector_swap(array, array2);
```



### 20.结构体成员读写数据

#### 方式一

调用接口`jh_vector_struct_at`对特定的结构体成员进行读写。动态数组读写数据的方式和操作普通数组的方式一致。

```c
/*读写数据*/
jh_vector_struct_at(vector, i, name);
```

参数含义：

| 参数   | 含义         |
| ------ | ------------ |
| vector | 动态数组     |
| i      | 索引值       |
| name   | 结构体成员名 |

使用示例展示对动态数组结构体成员读写数据：

```c
//循环赋值
for(int i = 0; i < 12; i++) {
    jh_vector_struct_at(array, i, key) = i;
    jh_vector_struct_at(array, i, val) = i + 10;
}

//打印出数组中的数组
for(int i = 0; i < jh_vector_size(array); i++) {
    printf("%d : key : %d, val : %d\n", i, jh_vector_struct_at(array, i, key), jh_vector_struct_at(array, i, val));   
}
```



#### 方式二

调用接口`jh_vector_at`加上`.name`对特定的结构体进行读写。动态数组读写数据的方式和操作普通数组的方式一致。

```c
/*读写数据*/
jh_vector_at(vector, i).name;
```

参数含义：

| 参数   | 含义         |
| ------ | ------------ |
| vector | 动态数组     |
| i      | 索引值       |
| name   | 结构体成员名 |

使用示例展示对动态数组结构体成员读写数据：

```c
//循环赋值
for(int i = 0; i < 12; i++) {
    jh_vector_at(array, i).key = i + 10;
    jh_vector_at(array, i).val = i;
}

//打印出数组中的数组
for(int i = 0; i < jh_vector_size(array); i++) {
    printf("%d : key : %d, val : %d\n", i, jh_vector_at(array, i).key, jh_vector_at(array, i).val);
}
```



### 21.获取结构体成员的地址

#### 方式一

调用接口`jh_vector_struct_data`获取动态数组结构体成员的地址。

```c
/*获取动态数组第i个数据的name成员的地址*/
jh_vector_struct_data(vector, i, name);
```

参数含义：

| 参数   | 含义         |
| ------ | ------------ |
| vector | 动态数组     |
| i      | 索引值       |
| name   | 结构体成员名 |

返回值：

| 返回值 | 含义                   |
| ------ | ---------------------- |
| ptr    | 返回指向数据地址的指针 |

使用示例：

```c
int *ptr = jh_vector_struct_data(array, 5, val);
printf("val : %d %d\n", *ptr, jh_vector_at(array, 5).val);
```



#### 方式二

调用接口`jh_vector_at`获取动态数组结构体成员再使用`&`取地址。

```c
/*获取动态数组第i个数据的name成员的地址*/
&(jh_vector_at(vector, i).name);
```

参数含义：

| 参数   | 含义         |
| ------ | ------------ |
| vector | 动态数组     |
| i      | 索引值       |
| name   | 结构体成员名 |

返回值：

| 返回值 | 含义                   |
| ------ | ---------------------- |
| ptr    | 返回指向数据地址的指针 |

使用示例：

```c
int *ptr = &(jh_vector_at(array, 6).val);
printf("val : %d %d\n", *ptr, jh_vector_at(array, 6).val);
```



### 22.动态数组重新初始化

调用接口`jh_vector_reinit`重新初始化动态数组。

```c
/*重新初始化动态数组，设置数组长度为n*/
jh_vector_reinit(vector, n);
/*重新初始化动态数组，设置数组长度为n，重新初始化数值为value*/
jh_vector_reinit(vector, n, value);
```

参数含义：

| 参数   | 含义                        |
| ------ | --------------------------- |
| vector | 动态数组                    |
| n      | 动态数组长度，必须大于等于0 |
| value  | 动态数组初始值              |

使用示例：

```c
//重新初始化动态数组，长度为10，初始值未知
jh_vector_reinit(array, 10);
//重新初始化动态数组，长度为10，初始值为8
jh_vector_reinit(array, 10, 8);
```



### 23.销毁动态数组

调用接口`jh_vector_destroy`销毁动态数组。

```c
/*销毁动态数组*/
jh_vector_destroy(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

使用示例：

```c
//销毁动态数组
jh_vector_destroy(array);
```



### 24.查看动态数组的容量

调用接口`jh_vector_capacity`查看动态数组的容量。

```c
/*查看动态数组的容量*/
jh_vector_capacity(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义               |
| ------ | ------------------ |
| len    | 返回动态数组的容量 |

使用示例：

```c
//查看动态数组的容量
printf("size : %d\n", jh_vector_capacity(array));
```



### 25.调整动态数组的容量

调用接口`jh_vector_reserve`调整动态数组的容量。

```c
/*调整动态数组的容量, 大小修改为n*/
jh_vector_reserve(vector, n);
```

参数含义：

| 参数   | 含义                        |
| ------ | --------------------------- |
| vector | 动态数组                    |
| n      | 动态数组容量，必须大于等于0 |

使用示例：

```c
//调整动态数组的容量
jh_vector_reserve(array, 16);
```



### 26.判断动态数组是否已满

调用接口`jh_vector_full`判断动态数组是否已满。

```c
/*判断动态数组是否已满*/
jh_vector_full(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

返回值：

| 返回值 | 含义                    |
| ------ | ----------------------- |
| true   | 动态数组已满时返回true  |
| false  | 动态数组未满时返回false |

使用示例：

```c
printf("array %s full\n", jh_vector_full(array) ? "is" : "isn't");
```



### 27.调整动态数组的容量使其与动态数组的大小相匹配

调用接口`jh_vector_shrink_to_fit`调整动态数组的容量使其与动态数组的大小相匹配。

```c
/*调整动态数组的容量使其与动态数组的大小相匹配*/
jh_vector_shrink_to_fit(vector);
```

参数含义：

| 参数   | 含义     |
| ------ | -------- |
| vector | 动态数组 |

使用示例：

```c
//查看动态数组的容量
printf("size : %d\n", jh_vector_capacity(array));
```



### 28.排序

调用接口`jh_vector_sort`对动态数组的数据进行排序。

```c
/*对动态数组的数据进行排序*/
jh_vector_sort(vector, sort, compare);
```

参数含义：

| 参数    | 含义                                                         |
| ------- | ------------------------------------------------------------ |
| vector  | 动态数组                                                     |
| sort    | 排序函数 `void qsort (void* base, size_t num, size_t size, int (*compar)(const void*,const void*));` |
| compare | 比较函数 `int compar (const void* p1, const void* p2);`      |

使用示例：

```c
//按从小到大排序
int compare(const void* a, const void* b) {
    return ( *(int *)a - *(int *)b );
}

//排序
jh_vector_sort(array, qsort, compare);
```



### 29.移除特定数据

调用接口`jh_vector_remove`移除动态数组中的特定数据。移除特定数据后不会修改动态数组的大小，会返回最后一个有效数据的下一个索引值。

```c
/*移除动态数组中的特定数据*/
jh_vector_remove(vector, first, last, value);
```

参数含义：

| 参数   | 含义                            |
| ------ | ------------------------------- |
| vector | 动态数组                        |
| first  | 起始的索引值，范围[first, last) |
| last   | 结束的索引值，范围[first, last) |
| value  | 要移除的数值                    |

返回值：

| 返回值 | 含义                               |
| ------ | ---------------------------------- |
| cursor | 返回最后一个有效数据的下一个索引值 |

使用示例：

```c
//假设插入数组数据为[0,1,3,3,4]
//移除特定数据3，返回最后一个有效数据的下一个索引值
size_t cursor = jh_vector_remove(array, 0, 5, 66);
printf("cursor %d\n", cursor);
//cursor为3
//打印出数组中的数值，变为[0,1,4]
for(int i = 0; i < cursor; i++) {
    printf("%d,", jh_vector_at(array, i));
}
```



### 30.移除重复数据

调用接口`jh_vector_unique`移除动态数组中的重复数据。移除重复数据后不会修改动态数组的大小，会返回最后一个有效数据的下一个索引值。使用该接口前需要先进行排序，使数据有序。

```c
/*移除动态数组中的重复数据*/
jh_vector_unique(vector, first, last);
```

参数含义：

| 参数   | 含义                            |
| ------ | ------------------------------- |
| vector | 动态数组                        |
| first  | 起始的索引值，范围[first, last) |
| last   | 结束的索引值，范围[first, last) |

返回值：

| 返回值 | 含义                               |
| ------ | ---------------------------------- |
| cursor | 返回最后一个有效数据的下一个索引值 |

使用示例：

```c
//假设插入数组数据为[1,1,3,3,4]
//移除特定数据3，返回最后一个有效数据的下一个索引值
size_t cursor = jh_vector_unique(array, 0, 5);
printf("cursor %d\n", cursor);
//cursor为3
//打印出数组中的数值，变为[1,3,4]
for(int i = 0; i < cursor; i++) {
    printf("%d,", jh_vector_at(array, i));
}
```



## License

MIT License

Copyright (c) 2022 Hong Jiahua

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
