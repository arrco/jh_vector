/*
 * Copyright (c) 2022 Hong Jiahua
 * https://gitee.com/arrco/jh_vector
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdio.h>

#include "jh_vector.h"
/************************************************************************/
/*                                                                      */
/************************************************************************/
#if (__WORDSIZE == 64)
    #define PAHD    "%ld"
#else
    #define PAHD    "%d"
#endif
/************************************************************************/
/*                                                                      */
/************************************************************************/
void jh_vector_basic_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //往数组末尾增加数值20
    jh_vector_push_back(array, 20);
    
    //打印出数组中的数组
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //删除数组的最后一个数据
    jh_vector_pop_back(array);

    //打印出数组中的数组
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");

    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
}

void jh_vector_define_init_test(void) {
//方式一
    //定义动态数组的数据类型int和变量名array，如果要正常使用还需要调用 jh_vector_init 进行初始化
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
//方式二
    //定义动态数组的数据类型int和变量名array2，并且进行初始化，长度为0，后续可以不再调用 jh_vector_init 初始化
    jh_vector_type_name(int, array2);
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array2));
    
//方式三
    //如果定义全局变量需要使用该接口，用法同 jh_vector_type
    jh_vector_global_type(int) array3;
    
    //初始化动态数组，大小为10
    jh_vector_init(array3, 10);
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array3));
    
    //如果使用jh_vector_global_type接口定义局部变量，在使用结束后必须使用 jh_vector_destroy 来销毁动态数组
    jh_vector_destroy(array3);
    
//方式四
    //如果定义全局变量需要使用该接口，用法同 jh_vector_type_name
    jh_vector_global_type_name(int, array4) ;
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array4));
    
    //如果使用 jh_vector_global_type_name 接口定义局部变量，在使用结束后必须使用 jh_vector_destroy 来销毁动态数组
    jh_vector_destroy(array4);
}

void jh_vector_data_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //将数组全部内容设置为0
    for(int i = 0; i < jh_vector_size(array); i++) {
        memset(jh_vector_data(array), 0x00, sizeof(int) * jh_vector_size(array));
    }
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //获取第5个元素的地址
    int* val = jh_vector_data(array, 5);
    *val = 88;
    
    //打印出第5个元素的数值
    printf("%d\n", jh_vector_at(array, 5));
    
    //获取第6个元素的地址
    val = &jh_vector_at(array, 6);
    *val = 66;
    
    //打印出第6个元素的数值
    printf("%d\n", jh_vector_at(array, 6));
}

void jh_vector_front_back_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //查看首元素的值
    //方式一
    printf("front1 %d\n", jh_vector_front(array));
    //方式二
    printf("front2 %d\n", jh_vector_at(array, 0));
    //方式三
    printf("front3 %d\n", jh_vector_at(array, jh_vector_begin(array)));
    //方式四
    printf("front4 %d\n", *jh_vector_data(array));
    
    //查看尾元素的值
    //方式一
    printf("back1 %d\n", jh_vector_back(array));
    //方式二
    printf("back2 %d\n", jh_vector_at(array, jh_vector_size(array) - 1));
    //方式三
    printf("back3 %d\n", jh_vector_at(array, jh_vector_end(array) - 1));
    //方式四
    printf("back4 %d\n", *jh_vector_data(array, jh_vector_end(array) - 1));
    
    //打印出数组中的数值
    for(int i = jh_vector_begin(array); i < jh_vector_end(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
}

void jh_vector_resize_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组
    jh_vector_init(array);
    //查看当前数组是否为空
    printf("array %s empty\n", jh_vector_empty(array) ? "is" : "isn't");
    
    /*调整动态数组的大小*/
    jh_vector_resize(array, 3, 66);
    
    //打印出数组中的数组
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //调整动态数组的大小
    jh_vector_resize(array, 8);
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //清空数组
    jh_vector_clear(array);
    
    //查看当前数组是否为空
    printf("array %s empty\n", jh_vector_empty(array) ? "is" : "isn't");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
}

void jh_vector_modify_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //修改第5个元素的数值
    jh_vector_modify(array, 5, 55);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //修改第12个元素的数值
    jh_vector_modify(array, 12, 55);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //修改范围为[5, 12)的元素的数值
    jh_vector_modify(array, 5, 12, 66);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //修改范围为[15, 18)的元素的数值
    jh_vector_modify(array, 15, 18, 77);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //修改范围为[10, 20)的元素的数值
    jh_vector_modify(array, 10, 20, 88);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
}

void jh_vector_insert_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //在索引值为5的位置上插入数据66
    jh_vector_insert(array, 5, 66);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //在索引值为5的位置上插入2个数据66
    jh_vector_insert(array, 5, 2, 77);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //定义动态数组的数据类型int和变量名array2
    jh_vector_type(int) array2;
    
    //初始化动态数组
    jh_vector_init(array2);
    
    //将动态数组array索引值范围为[1, 5)的数值，插入到动态数组array2索引值为0开始的位置
    jh_vector_insert(array2, 0, array, 1, 5);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array2); i++) {
        printf("%d,", jh_vector_at(array2, i));
    }
    printf("\n");
}

void jh_vector_erase_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //删除索引值为5的位置上的数据
    jh_vector_erase(array, 5);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //删除范围为[5, 7)的数据
    jh_vector_erase(array, 5, 7);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //删除数组的最后一个数据
    jh_vector_pop_back(array);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //删除数组的最后2个数据
    jh_vector_erase_num(array, 2);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //删除数组的从索引值为1开始后的所有数据
    jh_vector_erase_begin(array, 1);
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
}

void jh_vector_copy_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //定义动态数组的数据类型int和变量名array2
    jh_vector_type(int) array2;
    
    //初始化动态数组
    jh_vector_init(array2);
    
    //将array的所有数据复制到array2
    jh_vector_copy(array2, array);
    
    //打印出数组中的数值
    printf("array2:");
    for(int i = 0; i < jh_vector_size(array2); i++) {
        printf("%d,", jh_vector_at(array2, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array2));
    
    //定义动态数组的数据类型int和变量名array3
    jh_vector_type(int) array3;
    
    //初始化动态数组
    jh_vector_init(array3);
    
    //将array的前5个数据复制到array3
    jh_vector_copy(array3, array, 5);
    
    //打印出数组中的数值
    printf("array3:");
    for(int i = 0; i < jh_vector_size(array3); i++) {
        printf("%d,", jh_vector_at(array3, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array3));
    
    //定义动态数组的数据类型int和变量名array4
    jh_vector_type(int) array4;
    
    //初始化动态数组
    jh_vector_init(array4);
    
    //将array范围为[5, 8)的数据复制到array4
    jh_vector_copy(array4, array, 5, 8);
    
    //打印出数组中的数值
    printf("array4:");
    for(int i = 0; i < jh_vector_size(array4); i++) {
        printf("%d,", jh_vector_at(array4, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array4));
    
    //定义动态数组的数据类型int和变量名array5
    jh_vector_type(int) array5;
    
    //初始化动态数组
    jh_vector_init(array5);
    
    //将array范围为[5, 8)的数据复制到array5
    jh_vector_copy(array5, 0, array, 5, 8);
    
    //打印出数组中的数值
    printf("array5:");
    for(int i = 0; i < jh_vector_size(array5); i++) {
        printf("%d,", jh_vector_at(array5, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array5));
}

void jh_vector_swap_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array, array2;
    
    //初始化动态数组
    jh_vector_init(array, 10, 6);
    jh_vector_init(array2, 8, 20);
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //打印出数组中的数值
    printf("array2:");
    for(int i = 0; i < jh_vector_size(array2); i++) {
        printf("%d,", jh_vector_at(array2, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array2));
    
    //交换array和array2中的数据
    jh_vector_swap(array, array2);
    printf("swap\n");
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //打印出数组中的数值
    printf("array2:");
    for(int i = 0; i < jh_vector_size(array2); i++) {
        printf("%d,", jh_vector_at(array2, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array2));
}

void jh_vector_struct_test(void) {
    //定义结构体
    struct test{
        int key;
        int val;
    };
    
    //定义动态数组的数据类型struct test和变量名array
    jh_vector_type(struct test) array;
    
    //初始化动态数组
    jh_vector_init(array, 12);
    
    //读写方式一
        //循环赋值
        for(int i = 0; i < 12; i++) {
            jh_vector_struct_at(array, i, key) = i;
            jh_vector_struct_at(array, i, val) = i + 10;
        }
        
        //打印出数组中的数组
        for(int i = 0; i < jh_vector_size(array); i++) {
            printf("%d : key : %d, val : %d\n", i, jh_vector_struct_at(array, i, key), jh_vector_struct_at(array, i, val));
        }
    //读写方式二
        //循环赋值
        for(int i = 0; i < 12; i++) {
            jh_vector_at(array, i).key = i + 10;
            jh_vector_at(array, i).val = i;
        }
        
        //打印出数组中的数组
        for(int i = 0; i < jh_vector_size(array); i++) {
            printf("%d : key : %d, val : %d\n", i, jh_vector_at(array, i).key, jh_vector_at(array, i).val);
        }
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //删除数组的最后一个数据
    struct test tmpval = jh_vector_pop_back(array);
    printf("del key : %d, val : %d\n", tmpval.key, tmpval.val);
    
    //打印出数组中的数组
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d : key : %d, val : %d\n", i, jh_vector_at(array, i).key, jh_vector_at(array, i).val);
    }
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    //往数组末尾增加数值
    tmpval.key = 66;
    tmpval.val = 88;
    jh_vector_push_back(array, tmpval);
    
    //打印出数组中的数组
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d : key : %d, val : %d\n", i, jh_vector_at(array, i).key, jh_vector_at(array, i).val);
    }
    
    //查看当前数组大小
    printf("size : " PAHD "\n", jh_vector_size(array));
    
    int *tmpptr = jh_vector_struct_data(array, 5, val);
    printf("val : %d %d\n", *tmpptr, jh_vector_at(array, 5).val);
    *tmpptr = 55;
    printf("val : %d %d\n", *tmpptr, jh_vector_at(array, 5).val);
    
    tmpptr = &(jh_vector_at(array, 6).val);
    printf("val : %d %d\n", *tmpptr, jh_vector_at(array, 6).val);
    *tmpptr = 66;
    printf("val : %d %d\n", *tmpptr, jh_vector_at(array, 6).val);
}

void jh_vector_reinit_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //重新初始化动态数组
    jh_vector_reinit(array, 15, 20);
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //销毁动态数组
    jh_vector_destroy(array);
}

void jh_vector_capacity_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组容量
    printf("capacity : " PAHD "\n", jh_vector_capacity(array));
    
    //查看当前数组是否已满
    printf("array %s full\n", jh_vector_full(array) ? "is" : "isn't");
    
    //填满
    jh_vector_insert(array, 10, 6, 20);
    
    //查看当前数组是否已满
    printf("array %s full\n", jh_vector_full(array) ? "is" : "isn't");
    
    //查看当前数组容量
    printf("capacity : " PAHD "\n", jh_vector_capacity(array));
    
    //调整动态数组的容量
    jh_vector_reserve(array, 32);
    
    //查看当前数组是否已满
    printf("array %s full\n", jh_vector_full(array) ? "is" : "isn't");
    
    //查看当前数组容量
    printf("capacity : " PAHD "\n", jh_vector_capacity(array));
    
    //调整动态数组的容量使其与动态数组的大小相匹配
    jh_vector_shrink_to_fit(array);
    
    //查看当前数组容量
    printf("capacity : " PAHD "\n", jh_vector_capacity(array));
}

void jh_vector_remove_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //修改范围为[2, 7)的元素的数值
    jh_vector_modify(array, 2, 7, 66);
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //移除特定数据66，返回最后一个有效数据的下一个索引值
    size_t cursor = jh_vector_remove(array, 0, 9, 66);
    printf("cursor " PAHD "\n", cursor);
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < cursor; i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小，remove后数组长度不会改变
    printf("size : " PAHD "\n", jh_vector_size(array));
}

//按从小到大排序
int compare(const void* a, const void* b) {
    return ( *(int *)a - *(int *)b );
}

void jh_vector_sort_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = rand() % 100;
    }
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //方式一
    //按从小到大排序
    jh_vector_sort(array, qsort, compare);
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    printf("\n");
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = rand() % 100;
    }
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //方式二
    //按从小到大排序
    qsort(jh_vector_data(array), jh_vector_size(array), sizeof(int), compare);
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
}

void jh_vector_unique_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = rand() % 10;
    }
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //按从小到大排序
    jh_vector_sort(array, qsort, compare);
    
    //打印出数组中的数值
    printf("array:");
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //去除重复数据，返回最后一个有效数据的下一个索引值
    size_t cursor = jh_vector_unique(array, jh_vector_begin(array), jh_vector_end(array));
    printf("cursor " PAHD "\n", cursor);
    
    //打印出数组中的数值，unique后数组长度不会改变
    printf("array:");
    for(int i = 0; i < cursor; i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
}

void jh_vector_test(void) {
    //定义动态数组的数据类型int和变量名array
    jh_vector_type(int) array;
    
    //初始化动态数组，大小为10
    jh_vector_init(array, 10);
    
    //循环赋值
    for(int i = 0; i < jh_vector_size(array); i++) {
        jh_vector_at(array, i) = i;
    }
    
    //打印出数组中的数值
    for(int i = 0; i < jh_vector_size(array); i++) {
        printf("%d,", jh_vector_at(array, i));
    }
    printf("\n");
    
    //查看当前数组大小
    printf("size : %d\n", jh_vector_size(array));
    
    //往数组末尾增加数值20
    jh_vector_push_back(array, 20);
    
    //查看当前数组大小
    printf("size : %d\n", jh_vector_size(array));
    
    //删除数组的最后一个数据
    jh_vector_pop_back(array);

    //查看当前数组大小
    printf("size : %d\n", jh_vector_size(array));
}
/************************************************************************/
/*                                                                      */
/************************************************************************/
int main(int argc, char* argv[])
{
    //基础功能
    // jh_vector_basic_test();
    
    //定义与初始化
    // jh_vector_define_init_test();
    
    //指针操作
    // jh_vector_data_test();
    
    //首尾数据
    // jh_vector_front_back_test();
    
    //调整动态数组的大小
    // jh_vector_resize_test();
    
    //修改数据
    // jh_vector_modify_test();
    
    //插入数据
    // jh_vector_insert_test();
    
    //删除数据
    // jh_vector_erase_test();
    
    //复制数据
    // jh_vector_copy_test();
    
    //交换数据
    // jh_vector_swap_test();
    
    //操作结构体
    // jh_vector_struct_test();
    
    //重新初始化
    // jh_vector_reinit_test();
    
    //容量操作
    // jh_vector_capacity_test();
    
    //移除特定数据操作
    // jh_vector_remove_test();
    
    //排序操作
    // jh_vector_sort_test();
    
    //去重操作
    // jh_vector_unique_test();
    
    //使用示例
    jh_vector_test();
    
    return 0;
}